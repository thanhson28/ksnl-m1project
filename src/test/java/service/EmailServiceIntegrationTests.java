package service;

import com.puf.Application;
import com.puf.util.BookingEmailModel;
import com.puf.util.EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class EmailServiceIntegrationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceIntegrationTests.class);

    private static final String EMAIL_RECEIVE_ADDRESS = "phanthanhsont@gmail.com";

    @Autowired
    private EmailService emailService;

    @Test
    public void testSendEmail() {
        LOGGER.info("Test for sending email");
        emailService.sendEmail();
        LOGGER.info("Finish for sending email");
    }


    @Test
    public void testSendWelcomeEmail() {
        LOGGER.info("Test for sending mime welcome email");
        emailService.sendWelcomeEmail("phanthanhsont", EMAIL_RECEIVE_ADDRESS, "pwd@123456");
        LOGGER.info("Finish for sending email");
    }

    @Test
    public void testSendBookingEmail() {
        LOGGER.info("Test for sending mime booking email");

        final BookingEmailModel model = new BookingEmailModel();
        model.setCenterName("Center-1");
        model.setYardName("Yard-1");
        model.setSportName("Sport-Name-1");
        model.setScheduleDate("2019-08-07 15:04:02");
        model.setStartTime("08:12");
        model.setEndTime("12:30");

        emailService.sendBookingEmail(EMAIL_RECEIVE_ADDRESS, model);
        LOGGER.info("Finish for sending mime booking email");
    }
}