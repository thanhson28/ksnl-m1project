package com.puf.hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.puf.actor.Actor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.Calendar;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ActorControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetAllActors() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/actors").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Greetings from Spring Boot!")));
    }

    @Test
    public void testGetSpecificActor() throws Exception {
        final int actorId = 1;
        final String EXPECTED_ACTOR_JSONSTR = "{\n" +
                "    \"actorId\": 1,\n" +
                "    \"firstName\": \"PENELOPE\",\n" +
                "    \"lastName\": \"GUINESS\",\n" +
                "    \"lastUpdate\": \"2006-02-14T21:34:33.000+0000\"\n" +
                "}";
        mvc.perform(MockMvcRequestBuilders.get(String.format("/api/actor/%s", actorId)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(EXPECTED_ACTOR_JSONSTR, false));
    }

    @Test
    public void testCreateNewActor() throws Exception {
        insertNewActor();
    }

    private void insertNewActor() throws Exception {
        final Actor newActor = createNewActor();
        final ObjectMapper objectMapper = new ObjectMapper();
        final String actorJsonStr = objectMapper.writeValueAsString(newActor);

        //WHEN
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/actor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(actorJsonStr);

        final ResultActions resultActions = mvc.perform(builder).andExpect(status().isOk())
                .andExpect(content().json(actorJsonStr, false));

        //clean
        final MvcResult result = resultActions.andReturn();
        final String createdActorJson = result.getResponse().getContentAsString();
        final Actor createdActor = objectMapper.readValue(createdActorJson, Actor.class);
        final MockHttpServletRequestBuilder deleteBuilder = MockMvcRequestBuilders.delete(String.format("/api/actor/%s", createdActor.getActorId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        //perform delete
        mvc.perform(deleteBuilder).andExpect(status().isOk());
    }

    private Actor createNewActor() {
        final Actor newActor = new Actor();
        newActor.setFirstName("FirstName-test");
        newActor.setLastName("LastName-test");
        newActor.setLastUpdate(Calendar.getInstance().getTime());
        return newActor;
    }

    @Test
    public void tesDeleteSpecificActor() throws Exception {
        final int actorId = 201;
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete(String.format("/api/actor/%s", actorId))
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder).andExpect(status().isOk());
    }

    @Test
    public void testUpdateActor() throws Exception {
        final Actor newActor = createNewActor();
        final ObjectMapper objectMapper = new ObjectMapper();
        final String actorJsonStr = objectMapper.writeValueAsString(newActor);

        //GIVEN
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/actor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(actorJsonStr);

        final ResultActions resultActions = mvc.perform(builder).andExpect(status().isOk())
                .andExpect(content().json(actorJsonStr, false));
        final MvcResult result = resultActions.andReturn();
        final String createdActorJson = result.getResponse().getContentAsString();
        final Actor createdActor = objectMapper.readValue(createdActorJson, Actor.class);

        //WHEN
        createdActor.setFirstName("FirstName-Test-Update");
        createdActor.setLastName("LastName-Test-Update");
        final MockHttpServletRequestBuilder updateBuilder = MockMvcRequestBuilders.put(String.format("/api/actor/%s", createdActor.getActorId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(actorJsonStr);
        mvc.perform(updateBuilder).andExpect(status().isOk());


        //clean
        final MockHttpServletRequestBuilder deleteBuilder = MockMvcRequestBuilders.delete(String.format("/api/actor/%s", createdActor.getActorId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        //perform delete
        mvc.perform(deleteBuilder).andExpect(status().isOk());
    }
}
