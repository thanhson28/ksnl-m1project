package converter;

import com.puf.converter.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import java.time.LocalDateTime;

public class DateUtilTests {

    @Test
    public void testParseBirthDate() {
        final String birthDate = "1989-02-21";
        Assert.assertNotNull("Birthdate should be parsed", DateUtil.toDate(birthDate, DateUtil.SHORT_DATETIME_PATTERN));
    }

    @Test
    public void testParseStart_and_EndDate() {
        final String startDate = "2019-08-08 15:58:17";
        Assert.assertNotNull("Start datetime should be parsed", startDate);
    }
}
