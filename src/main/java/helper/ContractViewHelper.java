package helper;

import com.puf.contract.Contract;
import com.puf.contract.ContractStatus;
import com.puf.converter.DateUtil;
import com.puf.membership.Membership;
import com.puf.user_account.UserAccount;
import com.puf.view.ContractView;
import com.puf.view.ContractViewDto;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.Calendar;
import java.util.UUID;
import static com.puf.contract.Contract.DEFAULT_MAX_LAST_ACTION;
import static com.puf.converter.DateUtil.*;

/**
 * This is utility help to extract data
 */
public final class ContractViewHelper {

    private static final int PASSWORD_GEN_LENGTH = 12;

    public static UserAccount extractUserAccount(final ContractViewDto contractView) {
        final UserAccount userAccount = new UserAccount();
        userAccount.setUserName(contractView.getEmail());
        userAccount.setPassword(randomPwd());
        userAccount.setFirstName(contractView.getFirstName());
        userAccount.setLastName(contractView.getLastName());
        userAccount.setEmail(contractView.getEmail());
        userAccount.setDateOfBirth(toDate(contractView.getDateOfBirth(), SHORT_DATETIME_PATTERN));
        userAccount.setPhoneNumber(contractView.getPhone());
        userAccount.setAddress(contractView.getAddress());
        userAccount.setCity(contractView.getCity());
        userAccount.setCountry(contractView.getCountry());
        userAccount.setCategory(contractView.getAccountCategory());
        return userAccount;
    }

    private static String randomPwd() {
        final String randomPrefixPwd = RandomStringUtils.random(PASSWORD_GEN_LENGTH, true, true);
        final String randomUUID = UUID.randomUUID().toString().split("-")[0];
        return String.format("%s-%s", randomPrefixPwd, randomUUID);
    }

    public static Membership extractMembership(final ContractView contractView) {
        final Membership membership = new Membership();
        membership.setType(contractView.getMemberShipType());
        membership.setCardLevel(contractView.getCardLevel());
        membership.setSession(contractView.getSession());
        membership.setPrice(contractView.getPrice());
        membership.setNote("Create new membership by contractView");

        return membership;

    }

    public static Contract extractContract(final ContractViewDto contractView, final Long userAccountId, final Long membershipId) {
        final Contract contract = new Contract();
        contract.setUserAccountId(userAccountId);
        contract.setMembershipId(membershipId);
        contract.setStatus(ContractStatus.PENDING.getStatus());
        contract.setLastAction(DEFAULT_MAX_LAST_ACTION);
        contract.setStartDate(DateUtil.toDateTime(contractView.getStartDate(), DATETIME_PATTERN));
        contract.setEndDate(DateUtil.toDateTime(contractView.getEndDate(), DATETIME_PATTERN));

        contract.setNote(String.format("Create new contract by users and  membership %s", contractView.getUserAccountId(), membershipId));
        return contract;
    }
}
