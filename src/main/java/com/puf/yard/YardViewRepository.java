package com.puf.yard;

import com.puf.view.YardView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface YardViewRepository extends JpaRepository<YardView, Long> {
    List<YardView> findByUserAccountId(final Long userAccountId);
}
