package com.puf.yard;

import com.puf.ResourceNotFoundException;
import com.puf.center.Center;
import com.puf.login.UserAccountService;
import com.puf.schedule.Schedule;
import com.puf.schedule.ScheduleRepository;
import com.puf.schedule.ScheduleSlot;
import com.puf.user_account.UserAccount;
import com.puf.view.YardView;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class YardController {

    @Autowired
    private YardRepository yardRepository;

    @Autowired
    private YardViewRepository yardViewRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private UserAccountService userAccountService;

    /**
     * Get all yards.
     */
    @GetMapping("/api/yards")
    public List<Yard> getAllYards() {
        return yardRepository.findAll();
    }

    @GetMapping("/api/views/yards")
    public List<YardView> getAllYardViews() {
        return yardViewRepository.findAll();
    }

    @GetMapping("/api/views/yard")
    public List<YardView> getYardsByUserAccountId(@RequestParam("action") String action, @RequestHeader HttpHeaders headers) {
        final Optional<UserAccount> userAccount = userAccountService.getUserAccountFromTokenHeader(headers);
        if (!userAccount.isPresent()) {
            log.debug("Not found any user account");
            return Collections.emptyList();
        }

        final List<YardView> existingYardViews = yardViewRepository.findByUserAccountId(userAccount.get().getId());
        if (CollectionUtils.isEmpty(existingYardViews)) {
            log.debug(String.format("Not found any yard view by user %s", userAccount.get().getUserName()));
            return Collections.emptyList();
        }
        return existingYardViews;
    }

    @GetMapping("/api/views/yard/{id}")
    public YardView getYardViewById(@PathVariable(value = "id") Long yardId) {
        final YardView foundYardView = yardViewRepository.findById(yardId).orElseThrow(() -> new ResourceNotFoundException("Yard", "id", yardId));
        return foundYardView;
    }

    @GetMapping("/api/yard")
    public Integer countAlYard(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return yardRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such any action for this action");
    }

    /**
     * Create new {@link Yard}.
     */
    @PostMapping("/api/yard")
    public Yard createYard(@Valid @RequestBody Yard yard) {
        final Yard newYard = yardRepository.save(yard);

        for (final ScheduleSlot slot : ScheduleSlot.ALL_SLOTS) {
            createSlot(newYard.getId(), slot);
        }
        return newYard;
    }

    private Schedule createSlot(final long newYardId, final ScheduleSlot slot) {
        final Schedule newSchedule = new Schedule();
        newSchedule.setYardId(newYardId);
        newSchedule.setPartOfDay(slot.getPartOfDay());
        newSchedule.setStartTime(slot.getStartTime());
        newSchedule.setEndTime(slot.getEndTime());
        newSchedule.setStatus(Schedule.SCHEDULED);

        //save
        scheduleRepository.save(newSchedule);
        return newSchedule;
    }

    @GetMapping("/api/yard/{id}")
    public Yard getYardById(@PathVariable(value = "id") Long yardId) {
        final Yard foundYard = yardRepository.findById(yardId).orElseThrow(() -> new ResourceNotFoundException("Yard", "id", yardId));
        return foundYard;
    }


//    @PutMapping(name = "/api/yard/{id}")
//    public Yard updateYard(final @PathVariable(value = "id") Long id, @RequestBody Yard yard) {
//        final Yard existingYard = yardRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Yard", "id", id));
//
//        updateYard(yard, existingYard);
//
//        final Yard updatedYard = yardRepository.save(existingYard);
//        return updatedYard;
//    }

    /**
     * Update existing yard with new information.
     *
     * @param yard
     * @param existingYard
     */
    private void updateYard(final Yard yard, final Yard existingYard) {
        existingYard.setStatusId(yard.getStatusId());
//        existingYard.setSlot(yard.getSlot());
//        existingYard.setCloseDate(yard.getCloseDate());

        existingYard.setCenterId(yard.getCenterId());
        existingYard.setSportId(yard.getSportId());

//        existingYard.setUpdatedDate(Calendar.getInstance().getTime());
    }


    @DeleteMapping(value = "/yard/{id}")
    public ResponseEntity<?> deleteYard(@PathVariable(value = "id") Long id) {
        final Yard existingYard = yardRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Yard", "id", id));
        yardRepository.delete(existingYard);
        return ResponseEntity.ok().build();
    }

}
