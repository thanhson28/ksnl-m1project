package com.puf.yard;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "yard")
public class Yard {

    public Yard() {
    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotBlank
    private String name;

    @Column(name = "center_id")
    @NotNull
    private Integer centerId;

    @Column(name = "status_id")
    @NotBlank
    private String statusId;

    @Column(name = "sport_id")
    @NotNull
    private Integer sportId;

    @Column(name = "create_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime createDate;

    @Column(name = "valid")
    private Integer valid = 1;

    @Column(name = "insert_timestamp")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime insertTimestamp;


}
