package com.puf.yard;

import org.springframework.data.jpa.repository.JpaRepository;


public interface YardRepository extends JpaRepository<Yard, Long> {


}
