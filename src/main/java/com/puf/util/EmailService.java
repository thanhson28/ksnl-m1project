package com.puf.util;

import com.puf.exception.SportClubError;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class EmailService {

    private static final String WELCOME_TEMPLATE_FILE = "welcome.ftl";
    private static final String BOOKING_TEMPLATE_FILE = "booking.ftl";

    private static final String WELCOME_SUBJECT_TEXT = "Support-KSNL-Sport";
    private static final String BOOKING_SUBJECT_TEXT = "Booking in KSNL Sport Club";

    private static final String KSNL_LINK = "http://ksnl-sport.com";

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Configuration freeMarkerConfig;

    public void sendEmail() {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("phanthanhsont@gmail.com");
        msg.setSubject("Support-KSNL-Sport");
        msg.setText("Welcome , you just created a new account in Ksnl Sport club");

        emailSender.send(msg);
    }

    public void sendWelcomeEmail(final String userName, final String emailAddress, final String password) {
        final MimeMessage message = emailSender.createMimeMessage();

        final MimeMessageHelper helper = new MimeMessageHelper(message);

        final Map<String, Object> model = new HashMap<>();
        model.put("user_name", userName);
        model.put("user_email", emailAddress);
        model.put("password", password);
        model.put("link", KSNL_LINK);

        final Template template = loadTemplate(WELCOME_TEMPLATE_FILE);
        try {
            final String mappedText = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

            helper.setTo(emailAddress);
            helper.setSubject(WELCOME_SUBJECT_TEXT);
            helper.setText(mappedText, true); //enable HTML

            //send email
            emailSender.send(message);

        } catch (IOException | TemplateException | MessagingException e) {
            throw new SportClubError(String.format("Can not mapping template sending email %s", WELCOME_TEMPLATE_FILE));
        }

    }

    public void sendBookingEmail(final String emailAddress, final BookingEmailModel bookingModel) {
        final MimeMessage message = emailSender.createMimeMessage();

        final MimeMessageHelper helper = new MimeMessageHelper(message);

        final Map<String, Object> model = new HashMap<>();
        model.put("username", bookingModel.getUserName());
        model.put("center_name", bookingModel.getCenterName());
        model.put("yard_name", bookingModel.getYardName());
        model.put("sport_name", bookingModel.getSportName());
        model.put("schedule_date", bookingModel.getScheduleDate());
        model.put("start_time", bookingModel.getStartTime());
        model.put("end_time", bookingModel.getEndTime());
        model.put("link", KSNL_LINK);


        final Template template = loadTemplate(BOOKING_TEMPLATE_FILE);
        try {
            final String mappedText = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

            helper.setTo(emailAddress);
            helper.setSubject(BOOKING_SUBJECT_TEXT);
            helper.setText(mappedText, true); //enable HTML

            //send email
            emailSender.send(message);

        } catch (IOException | TemplateException | MessagingException e) {
            throw new SportClubError(String.format("Can not mapping template sending email %s", WELCOME_TEMPLATE_FILE));
        }

    }

    /**
     * set loading location to src/main/resources
     * You may want to use a subfolder such as /templates here
     *
     * @param templateFile
     * @return
     */
    private Template loadTemplate(final String templateFile) {
        freeMarkerConfig.setClassForTemplateLoading(this.getClass(), "/");
        try {
            return freeMarkerConfig.getTemplate(templateFile);
        } catch (IOException e) {
            throw new SportClubError(String.format("Can not load template sending email %s", WELCOME_TEMPLATE_FILE));
        }
    }

}
