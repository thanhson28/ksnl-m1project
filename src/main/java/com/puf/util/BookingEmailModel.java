package com.puf.util;

import lombok.Data;

@Data
public class BookingEmailModel {

    private String userName;

    private String centerName;

    private String yardName;

    private String sportName;

    private String scheduleDate;

    private String startTime;

    private String endTime;
}
