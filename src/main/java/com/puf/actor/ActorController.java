package com.puf.actor;

import com.puf.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ActorController {

    @Autowired
    private ActorRepository actorRepository;

    /**
     * Get all actors.
     * /api/actors
     */
    @GetMapping("/actors")
    public List<Actor> getAllActors() {
        return actorRepository.findAll();
    }

    @GetMapping("/actor")
    public Integer countAllActors(@RequestParam("action") String action) {
        if("count".equals(action)) {
            return actorRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such any action for this action");
    }

    /**
     * Create new com.puf.actor.
     * /api/actor
     */

    @PostMapping("/actor")
    public Actor createActor(@Valid @RequestBody Actor actor) {
        return actorRepository.save(actor);
    }

    /**
     * Get a single com.puf.actor
     * /api/actor/{id}
     */
    @GetMapping("/actor/{id}")
    public Actor getActorById(@PathVariable(value = "id") Long actorId) {
        final Actor foundActor = actorRepository.findById(actorId).orElseThrow(() -> new ResourceNotFoundException("Actor", "id",  actorId));
        return foundActor;
    }

    /**
     * Update an com.puf.actor.
     * /api/actor
     */
    public Actor updateActor(@PathVariable(value = "id") Long id, @RequestBody Actor actor) {
        final Actor existingActor = actorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Actor", "id", actor));
        existingActor.setFirstName(actor.getFirstName());
        existingActor.setLastName(actor.getLastName());
        existingActor.setLastUpdate(Calendar.getInstance().getTime());

        final Actor updatedActor = actorRepository.save(existingActor);
        return updatedActor;
    }


    /**
     * Delete an com.puf.actor.
     * /api/actor
     */
    @DeleteMapping(value = "/actor/{id}")
    public ResponseEntity<?> deleteActor(@PathVariable(value = "id") Long id) {
        final Actor exisitingActor = actorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Actor", "id", id));
        actorRepository.delete(exisitingActor);
        return ResponseEntity.ok().build();
    }

}
