package com.puf.login;

import lombok.Data;
import java.io.Serializable;

@Data
public class LoginRequest implements Serializable {
    private static final long serialVersionUID = -2869948624809419962L;

    public LoginRequest() {

    }

    private String email;
    private String password;
}
