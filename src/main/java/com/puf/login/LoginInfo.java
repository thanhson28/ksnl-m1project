package com.puf.login;

import lombok.Data;
import java.io.Serializable;

@Data
public class LoginInfo implements Serializable {
    private static final long serialVersionUID = -5463205605117162082L;

    private String message;
    private UserLoginDTO data;

    public LoginInfo() {
    }

    public LoginInfo(final String message) {
        this.message = message;
    }

}
