package com.puf.login;

import com.puf.user_account.UserAccount;
import com.puf.user_account.UserAccountRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserAccountService {

    public static final String PREFIX_BEARER_HEADER = "Bearer";


    @Autowired
    private UserAccountRepository userAccountRepository;

    public Optional<UserAccount> login(String username, String password) {
        Optional<UserAccount> userAccount = userAccountRepository.login(username, password);
        if (userAccount.isPresent()) {
            UserAccount existUserAcc = userAccount.get();
            if (StringUtils.isEmpty(existUserAcc.getToken())) {
                //generate token
                String token = UUID.randomUUID().toString();
                existUserAcc.setToken(token);
            }

            userAccountRepository.save(existUserAcc);
            return Optional.of(existUserAcc);
        }

        return Optional.empty();
    }

    public Optional<User> findByToken(String token) {
        Optional<UserAccount> customer = userAccountRepository.findByToken(token);
        if (customer.isPresent()) {
            UserAccount existUserAccount = customer.get();

            User user = new User(existUserAccount.getUserName(), existUserAccount.getPassword(), true, true, true, true,
                    AuthorityUtils.createAuthorityList("USER"));
            return Optional.of(user);
        }
        return Optional.empty();
    }

    public UserAccount findById(Long id) {
        Optional<UserAccount> userAccount = userAccountRepository.findById(id);
        return userAccount.orElse(null);
    }

    public Optional<UserAccount> getUserAccountFromTokenHeader(final HttpHeaders headers) {
        final List<String> authorizationHeaders = headers.get(HttpHeaders.AUTHORIZATION);
        if (CollectionUtils.isEmpty(authorizationHeaders)) {
            return Optional.empty();
        }

        final String tokenHeader = authorizationHeaders.get(0);
        final String token = StringUtils.removeStart(tokenHeader, PREFIX_BEARER_HEADER).trim();
        return userAccountRepository.findByToken(token);

    }

}
