package com.puf.login;

import com.puf.user_account.UserAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@Slf4j
public class LoginController {

    private static final String TOKEN_EXIST_MSG = "Token existing";
    private static final String LOGIN_SUCCESSFUL = "Login successful";

    @Autowired
    private UserAccountService userAccountService;

    @PostMapping("/token")
    public LoginInfo getToken(@RequestParam("email") final String email, @RequestParam("password") final String password) {
        Optional<UserAccount> userAcc = userAccountService.login(email, password);
        if (!userAcc.isPresent()) {
            return new LoginInfo("No token found");
        }

        return toLoginInfo(userAcc.get(), TOKEN_EXIST_MSG);
    }

    @PostMapping(name = "/login")
    public LoginInfo loginWithAuthorization(@Valid LoginRequest loginRequest) {
        final Optional<UserAccount> userAccount = userAccountService.login(loginRequest.getEmail(), loginRequest.getPassword());
        if (!userAccount.isPresent()) {
            return new LoginInfo("Login information is invalid");
        }

        return toLoginInfo(userAccount.get(), LOGIN_SUCCESSFUL);

    }

    private LoginInfo toLoginInfo(final UserAccount userAccount, final String message) {
        final UserLoginDTO userDto = new UserLoginDTO();
        userDto.setUserId(userAccount.getId());
        userDto.setUserName(userAccount.getUserName());
        userDto.setEmail(userAccount.getEmail());
        userDto.setFirstName(userAccount.getFirstName());
        userDto.setLastName(userAccount.getLastName());
        userDto.setToken(userAccount.getToken());
        userDto.setValid(userAccount.getValid());
        userDto.setCategory(userAccount.getCategory());

        final LoginInfo loginInfo = new LoginInfo();
        loginInfo.setMessage(message);
        loginInfo.setData(userDto);
        return loginInfo;
    }

}
