package com.puf.login;

import lombok.Data;
import java.io.Serializable;

@Data
public class UserLoginDTO implements Serializable {
    private static final long serialVersionUID = 1601431879550325922L;


    public UserLoginDTO() {

    }

    private Long userId;
    private String userName;
    private String email;
    private String firstName;
    private String lastName;
    private String token;
    private Integer valid;
    private Integer category;
}
