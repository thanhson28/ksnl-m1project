package com.puf.team_group;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TeamGroupRepository extends JpaRepository<TeamGroup, Long> {

    @Query(value = "SELECT * FROM team_group t where t.group_name = ?0", nativeQuery=true)
    List<TeamGroup> findByGroupName(String groupName);

}
