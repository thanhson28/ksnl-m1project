package com.puf.team_group;

import com.puf.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

/**
 * This is controller of model {@link TeamGroup}.
 */
@Slf4j
@RestController
public class TeamGroupController {

    @Autowired
    private TeamGroupRepository teamGroupRepository;

    /**
     * Get all team groups
     */
    @GetMapping("/api/teamgroups")
    public List<TeamGroup> getAllTeamGroups() {
        return teamGroupRepository.findAll();
    }

    @GetMapping("/api/teamgroup")
    public Integer countAllTeamGroups(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return teamGroupRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such any action for this action");
    }

    /**
     * Create new team group
     */
    @PostMapping("/api/teamgroup")
    public TeamGroup createTeamGroup(@Valid @RequestBody TeamGroup teamGroup) {
        return teamGroupRepository.save(teamGroup);
    }

    /**
     * Get a single team group
     */
    @GetMapping("/api/teamgroup/{id}")
    public TeamGroup getTeamGroupById(@PathVariable(value = "id") Long id) {
        final TeamGroup existingTeamGroup = teamGroupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Team Group", "id", id));
        return existingTeamGroup;
    }

    @GetMapping("/api/teamgroup/search/{groupName}")
    public List<TeamGroup> searchTeamGroup(@PathVariable(value = "groupName") String groupName) {
        final List<TeamGroup> existingTeamGroups = teamGroupRepository.findByGroupName(groupName);
        if (CollectionUtils.isEmpty(existingTeamGroups)) {
            log.debug("Empty existing team group");
        }
        return existingTeamGroups;
    }

//    /**
//     * Update an {@link TeamGroup}.
//     */
//    @PutMapping(name = "/api/teamgroup/{id}")
//    public TeamGroup updateTeamGroup(@PathVariable(value = "id") Long id, @RequestBody TeamGroup teamGroup) {
//        final TeamGroup existingTeamGroup = teamGroupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Team Group", "id", id));
//        existingTeamGroup.setGroupName(teamGroup.getGroupName());
//        existingTeamGroup.setUpdatedDate(Calendar.getInstance().getTime());
//
//        final TeamGroup updatedTeamGroup = teamGroupRepository.save(existingTeamGroup);
//        return updatedTeamGroup;
//    }

    @DeleteMapping(value = "/api/teamgroup/{id}")
    public ResponseEntity<?> deleteTeamGroup(@PathVariable(value = "id") Long id) {
        final TeamGroup existingTeamGroup = teamGroupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Team Group", "id", id));
        teamGroupRepository.delete(existingTeamGroup);
        return ResponseEntity.ok().build();
    }
}
