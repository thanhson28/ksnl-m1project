package com.puf.team_group;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "team_group")
public class TeamGroup implements Serializable {

    public TeamGroup() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teamGroupId;

    @Column(name = "group_name")
    @NotBlank
    private String groupName;

    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;


}
