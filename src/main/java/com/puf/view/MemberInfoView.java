package com.puf.view;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "member_info_view")
@Data
public class MemberInfoView  {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "date_of_birth")
  private Instant dob;

  @Column(name = "phone")
  private String phone;

  @Column(name = "address")
  private String address;

  @Column(name = "city")
  private String city;

  @Column(name = "country")
  private String country;

  @Column(name = "membership_type")
  private String membershipT;

  @Column(name = "card_level")
  private String cardLevel;

  @Column(name = "session")
  private Integer session;

  @Column(name = "price")
  private Integer price;

  @Column(name = "currency")
  private String currency;

  @Column(name = "start_date")
  private Instant startDate;

  @Column(name = "end_date")
  private Instant endDate;

  @Column(name = "total_contract")
  private Integer totalContract;

}
