package com.puf.view;

import lombok.Data;
import java.io.Serializable;

@Data
public class ContractViewDto implements Serializable {

    private Long userAccountId;

    private String firstName;

    private String lastName;

    private String email;

    private String dateOfBirth;

    private String phone;

    private String address;

    private String city;

    private String country;

    private Integer accountCategory;

    //membership
    private Long membershipId;

    //contract
    private String startDate;

    private String endDate;
}
