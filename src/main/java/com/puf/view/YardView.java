package com.puf.view;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "yard_view")
@Data
public class YardView  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "yard_id")
    private Long yard_id;

    @Column(name = "sport_id")
    private Long sportId;

    @Column(name = "center_id")
    private Long centerId;

    @Column(name = "sport_name")
    private String sport_name;

    @Column(name = "slot")
    private Integer slot;

    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "center_name")
    private String centerName;

    @Column(name = "time_open")
    private String timeOpen;

    @Column(name = "time_close")
    private String timeClose;

    @Column(name = "phone")
    private String phone;

    @Column(name = "website")
    private String website;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "valid")
    private String valid;

    @Column(name = "status_name")
    private String status_name;

}
