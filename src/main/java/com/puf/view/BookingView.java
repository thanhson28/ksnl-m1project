package com.puf.view;

import lombok.Data;
import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "booking_view")
@Data
public class BookingView {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)

  @Column(name = "booking_id")
  private Long bookingId;

  @Column(name = "player_id")
  private Long playerId; // fk

  @Column(name = "owner_id")
  private Long ownerId;; // fk

  @Column(name = "user_name")
  private String userName; //fk

  @Column(name = "yard_name")
  private String yardName;

  @Column(name = "sport_name")
  private String SportName;

  @Column(name = "slot")
  private Integer slot;

  @Column(name = "booked")
  private Integer bookedStatus;

  @Column(name = "schedule_date")
  private Instant scheduleDate;

  @Column(name = "start_time")
  private String startDate;

  @Column(name = "end_time")
  private String endDate;

  @Column(name = "center_name")
  private String centerName;

  @Column(name = "phone")
  private String phone;

  @Column(name = "website")
  private String website;

  @Column(name = "sport_image")
  private String sportImage;

  @Column(name = "address")
  private String address;

  @Column(name = "city")
  private String city;

  @Column(name = "country")
  private String country;

  @Column(name = "booking_status")
  private Integer bookingStatus;

}
