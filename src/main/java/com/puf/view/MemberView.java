package com.puf.view;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "member_view")
@Data
public class MemberView  {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "date_of_birth")
  private Instant dob;

  @Column(name = "phone")
  private String phone;

  @Column(name = "address")
  private String address;

  @Column(name = "city")
  private String city;

  @Column(name = "country")
  private String country;

  @Column(name = "valid")
  private String valid;

  @Column(name = "created_date")
  private Instant createdDate;

}
