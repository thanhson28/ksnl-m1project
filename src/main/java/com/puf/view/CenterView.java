package com.puf.view;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "center_view")
@Data
public class CenterView  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "time_open")
    private String timeOpen;

    @Column(name = "time_close")
    private String timeClose;

    @Column(name = "phone")
    private String phone;

    @Column(name = "website")
    private String website;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "valid")
    private String valid;

    @Column(name = "status_name")
    private String status_name;
}
