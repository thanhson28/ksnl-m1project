package com.puf.view;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "membership_view")
public class MembershipView {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "membership_id")
  private Long id;

  @Column(name = "type")
  private String type;

  @Column(name = "card_level")
  private String cardLevel;

  @Column(name = "session")
  private int session;

  @Column(name = "price")
  private int price;

  @Column(name = "currency")
  private String currency;

  @Column(name = "note")
  private String note;
}
