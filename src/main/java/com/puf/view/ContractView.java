package com.puf.view;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

@Entity
@Table(name = "contract_view")
@Data
public class ContractView implements Serializable {

    private static final long serialVersionUID = -4958112895796124447L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long contract_id;

    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "membership_id")
    private Long membershipId;

    @Column(name = "membership_type")
    private String memberShipType;

    @Column(name = "card_level")
    private String cardLevel;

    @Column(name = "session")
    private int session;

    @Column(name = "price")
    private int price;

    @Column(name = "currency")
    private String currency;

    @Column(name = "start_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime startDate;

    @Column(name = "end_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime endDate;

    @Column(name = "created_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime createDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "sent_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime sentDate;

    @Column(name = "sent_by")
    private String sentBy;

    @Column(name = "approved_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime approvedDate;

    @Column(name = "approved_by")
    private String approvedBy;

    @Column(name = "signed_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime signedDate;

    @Column(name = "signed_by")
    private String signedBy;

    @Column(name = "rejected_date")
    @Convert(converter = DateTimeConverter.class)
    private Instant rejectedDate;

    @Column(name = "rejected_by")
    private String rejectedBy;

}
