package com.puf.view;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "schedule_view")
@Data
public class ScheduleView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @Column(name = "yard_id")
    private Integer yardId;

    @Column(name = "yard_name")
    private String yardName;

    @Column(name = "sport_name")
    private String sportName;

    @Column(name = "slot")
    private Integer slot;

    @Column(name = "booked")
    private Integer booked;

    @Column(name = "sport_image")
    private String sportImage;

    @Column(name = "schedule_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime scheduleDate;

    @Column(name = "start_time")
    private String startTime;

    @Column(name = "end_time")
    private String endTime;

    @Column(name = "schedule_status")
    private String scheduleStatus;

    @Column(name = "center_name")
    private String centerName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "website")
    private String website;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

}
