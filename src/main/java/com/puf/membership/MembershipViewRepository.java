package com.puf.membership;

import com.puf.view.MembershipView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipViewRepository extends JpaRepository<MembershipView, Long> {
}
