package com.puf.membership;

import com.puf.ResourceNotFoundException;
import com.puf.view.MembershipView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
public class MembershipController {

    @Autowired
    private MembershipRepository membershipRepository;

    @Autowired
    private MembershipViewRepository membershipViewRepo;

    @GetMapping("/api/memberships")
    public List<Membership> getAllMemberships() {
        return membershipRepository.findAll();
    }

    @GetMapping("/views/memberships")
    public List<MembershipView> getAllMembershipViews() {
        return membershipViewRepo.findAll();
    }

    @GetMapping("/api/membership")
    public Integer countAllMembership(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return membershipRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/membership")
    public Membership createMembership(@Valid @RequestBody Membership membership) {
        return membershipRepository.save(membership);
    }


    @GetMapping("/api/membership/{id}")
    public Membership getMembershipById(@PathVariable(value = "id") Long membershipId) {
        final Membership foundMembership = membershipRepository.findById(membershipId).orElseThrow(() -> new ResourceNotFoundException("Membership", "id", membershipId));
        return foundMembership;
    }

    @PutMapping("/api/membership/{id}")
    public Membership updateMembership(@PathVariable(value = "id") Long id, @RequestBody Membership membership) {
        final Membership existingMembership = membershipRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Membership", "id", id));
        existingMembership.setType(membership.getType());
        existingMembership.setSession(membership.getSession());
        existingMembership.setPrice(membership.getPrice());
        existingMembership.setNote(membership.getNote());
        existingMembership.setUpdatedDate(Calendar.getInstance().getTime());

        final Membership updatedMembership = membershipRepository.save(existingMembership);
        return updatedMembership;
    }

    @DeleteMapping(value = "/api/membership/{id}")
    public ResponseEntity<?> deleteMembership(@PathVariable(value = "id") Long id) {
        final Membership exisitingMembership = membershipRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Membership", "id", id));
        membershipRepository.delete(exisitingMembership);
        return ResponseEntity.ok().build();
    }
}
