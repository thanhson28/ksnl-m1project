package com.puf.membership;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "membership")
public class Membership implements Serializable {

    public Membership() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    @NotBlank
    private String type;

    @Column(name = "card_level")
    @NotBlank
    private String cardLevel;

    @Column(name = "session")
    @NotBlank
    private Integer session;

    @Column(name = "price")
    @NotBlank
    private Integer price;

    @Column(name = "note")
    @NotBlank
    private String note;

    @Column(name = "insert_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

}
