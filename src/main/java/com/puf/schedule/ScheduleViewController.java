package com.puf.schedule;

import com.puf.view.ScheduleView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class ScheduleViewController {

    @Autowired
    private ScheduleViewRepository scheduleViewRepo;

    @GetMapping("/views/schedules")
    public List<ScheduleView> getAllScheduleViews() {
        return scheduleViewRepo.findAll();
    }

    @GetMapping("/api/views/schedule/{scheduleId}")
    public Optional<ScheduleView> getScheduleViewById(@PathVariable final Integer scheduleId) {
        return scheduleViewRepo.findByScheduleId(scheduleId);
    }

}
