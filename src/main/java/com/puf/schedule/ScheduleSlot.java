package com.puf.schedule;

import com.google.common.collect.Lists;
import java.util.List;

public enum ScheduleSlot {

    BEGIN(1, "8:00:00", "11:00:00"),
    MIDDLE(2, "13:00:00", "16:00:00"),
    END(3, "17:00:00", "20:00:00");

    public static final  List<ScheduleSlot> ALL_SLOTS = Lists.newArrayList(BEGIN, MIDDLE, END);

    private int partOfDay;
    private String startTime;
    private String endTime;

    private ScheduleSlot(final int partOfDay, final String startTime, final String endTime) {
        this.partOfDay = partOfDay;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getPartOfDay() {
        return partOfDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
