package com.puf.schedule;

import com.puf.view.ScheduleView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface ScheduleViewRepository extends JpaRepository<ScheduleView, Integer> {

    @Query(value = "SELECT sch FROM ScheduleView sch where sch.scheduleId = ?1")
    Optional<ScheduleView> findByScheduleId(final Integer scheduleId);
}
