package com.puf.schedule;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

    public static final int SCHEDULED = 1;
    public static final int CANCELLED = 2;
    public static final int FULLED = 3;
    public static final int FINISHED = 4;

    public Schedule() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "yard_id")
    @NotNull
    private Long yardId;

    @Column(name = "part_of_day")
    @NotNull
    private Integer partOfDay;

    @Column(name = "schedule_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime scheduleDate;

    @Column(name = "start_time")
    @NotBlank
    private String startTime;

    @Column(name = "end_time")
    @NotBlank
    private String endTime;

    @Column(name = "schedule_status")
    @NotNull
    private Integer status;

    @Column(name = "updated_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime updatedDate;

}
