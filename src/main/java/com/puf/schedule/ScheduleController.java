package com.puf.schedule;

import com.puf.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
public class ScheduleController {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @GetMapping("/schedules")
    public List<Schedule> getAllSchedules() {
        return scheduleRepository.findAll();
    }

    @GetMapping("/api/schedule")
    public Integer countAllSchedule(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return scheduleRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/schedule")
    public Schedule createSchedule(@Valid @RequestBody Schedule schedule) {
        return scheduleRepository.save(schedule);
    }


    @GetMapping("/api/schedule/{id}")
    public Schedule getScheduleById(@PathVariable(value = "id") Long id) {
        final Schedule schedule = scheduleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Schedule", "id", id));
        return schedule;
    }

    @PutMapping("/api/schedule/{id}")
    public Schedule updateSchedule(@PathVariable(value = "id") Long id, @RequestBody Schedule schedule) {
        final Schedule existingSchedule = scheduleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Schedule", "id", id));

        existingSchedule.setYardId(schedule.getYardId());
        existingSchedule.setPartOfDay(schedule.getPartOfDay());
        existingSchedule.setScheduleDate(schedule.getScheduleDate());
        existingSchedule.setStartTime(schedule.getStartTime());
        existingSchedule.setEndTime(schedule.getEndTime());
        existingSchedule.setStatus(schedule.getStatus());
        existingSchedule.setUpdatedDate(schedule.getUpdatedDate());

        final Schedule updatedSchedule = scheduleRepository.save(existingSchedule);
        return updatedSchedule;
    }

    @DeleteMapping(value = "/api/schedule/{id}")
    public ResponseEntity<?> deleteSchedule(@PathVariable(value = "id") Long id) {
        final Schedule exisitingSchedule = scheduleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Schedule", "id", id));
        scheduleRepository.delete(exisitingSchedule);
        return ResponseEntity.ok().build();
    }

}
