package com.puf.booking;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "booking")
public class Booking implements Serializable {

    public Booking() {
    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_account_id")
    private Integer userAccount;

    @Column(name = "schedule_id")
    private Integer scheduleId;

    @Column(name = "booked_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime bookedDate;

    @Column(name = "cancel_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime cancelDate;

    @Column(name = "booking_status")
    private Integer status = 1;

    @Column(name = "insert_timestamp")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime updatedDate = LocalDateTime.now();


}
