package com.puf.booking;

import com.puf.view.BookingView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingViewRepository extends JpaRepository<BookingView, Long> {
    List<BookingView> findByPlayerId(final Long userAccountId);
}
