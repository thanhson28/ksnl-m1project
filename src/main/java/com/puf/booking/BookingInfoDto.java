package com.puf.booking;

import lombok.Data;
import java.io.Serializable;

@Data
public class BookingInfoDto implements Serializable {

    private static final long serialVersionUID = 1961145158469538655L;

    public BookingInfoDto() {

    }

    public BookingInfoDto(final String message, final Booking booking) {
        this.message = message;
        this.booking = booking;
    }

    public BookingInfoDto(final String msg) {
        this.message = msg;
    }

    private String message;
    private Booking booking;

}
