package com.puf.booking;

import com.puf.ResourceNotFoundException;
import com.puf.login.UserAccountService;
import com.puf.schedule.Schedule;
import com.puf.schedule.ScheduleRepository;
import com.puf.schedule.ScheduleViewRepository;
import com.puf.user_account.UserAccount;
import com.puf.user_account.UserAccountRepository;
import com.puf.util.BookingEmailModel;
import com.puf.util.EmailService;
import com.puf.view.BookingView;
import com.puf.view.ScheduleView;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class BookingController {

    private final String BOOKING_MSG = "Booking";

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingViewRepository bookingViewRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleViewRepository scheduleViewRepository;

    @Autowired
    private UserAccountRepository userAccountRepo;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserAccountService userAccountService;

    @GetMapping("/api/bookings")
    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }

//    @GetMapping("/api/views/bookings")
//    public List<BookingView> getAllBookingViews() {
//        return bookingViewRepository.findAll();
//    }

    @GetMapping("/api/views/bookings")
    public List<BookingView> getBookingViewsByUserAccountId(@RequestParam("action") String action, @RequestHeader HttpHeaders headers) {
        final Optional<UserAccount> userAccount = userAccountService.getUserAccountFromTokenHeader(headers);
        if (!userAccount.isPresent()) {
            log.debug("Not found any user account");
            return Collections.emptyList();
        }

        final List<BookingView> existingBookingViews = bookingViewRepository.findByPlayerId(userAccount.get().getId());
        if (CollectionUtils.isEmpty(existingBookingViews)) {
            log.debug(String.format("Not found any booking view by user %s", userAccount.get().getUserName()));
            return Collections.emptyList();
        }

        return existingBookingViews;
    }

    @GetMapping("/api/views/booking/{id}")
    public BookingView getBookingViewById(@PathVariable(value = "id") Long bookingId) {
        final BookingView foundBookingView = bookingViewRepository.findById(bookingId).orElseThrow(() -> new ResourceNotFoundException("Booking", "id", bookingId));
        return foundBookingView;
    }

    @GetMapping("/api/booking")
    public Integer countAllBooking(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return bookingRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/booking")
    public BookingInfoDto createBooking(@Valid @RequestBody Booking booking) {

        final Integer scheduleId = booking.getScheduleId();
        final Optional<ScheduleView> scheduleView = scheduleViewRepository.findById(scheduleId);

        if (!scheduleView.isPresent()) {
            booking.setUserAccount(Integer.valueOf(booking.getUserAccount()));
            final Booking newBooking = bookingRepository.save(booking);

            //sending Email
            sendEmail(Long.valueOf(booking.getUserAccount().toString()), scheduleView.get());

            return new BookingInfoDto("Save new Booking successful", newBooking);
        }

        if (lessThanMaxSlot(scheduleView)) {

            if (isFullBooked(scheduleView)) {
                final Optional<Schedule> existSchedule = scheduleRepository.findById(Long.valueOf(Integer.toString(scheduleId)));
                final Schedule foundSchedule = existSchedule.get();
                foundSchedule.setStatus(Schedule.FULLED);

                //mark schedule as FULL booking within slot number
                log.debug("Mark schedule as FULL booking slot");
                scheduleRepository.save(foundSchedule);
            }


            booking.setUserAccount(booking.getUserAccount());
            final Booking newBooking = bookingRepository.save(booking);

            //sending Email
            sendEmail(Long.valueOf(booking.getUserAccount()), scheduleView.get());

            return new BookingInfoDto("Save new Booking successful", newBooking);
        }



        return new BookingInfoDto("Can not booking because of maximum slot already");
    }

    private void sendEmail(final Long userAccountId, final ScheduleView scheduleView ) {
        final Optional<UserAccount> userAccount = userAccountRepo.findById(userAccountId);
        final String emailAddress = userAccount.get().getEmail();

        final BookingEmailModel model = new BookingEmailModel();
        model.setUserName(userAccount.get().getUserName());
        model.setCenterName(scheduleView.getCenterName());
        model.setYardName(scheduleView.getYardName());
        model.setSportName(scheduleView.getSportName());
        model.setScheduleDate(scheduleView.getScheduleDate().toString());
        model.setStartTime(scheduleView.getStartTime());
        model.setEndTime(scheduleView.getEndTime());

        emailService.sendBookingEmail(emailAddress, model);
    }


    private boolean isFullBooked(Optional<ScheduleView> scheduleView) {
        return scheduleView.get().getBooked() == scheduleView.get().getSlot();
    }

    private boolean lessThanMaxSlot(Optional<ScheduleView> scheduleView) {
        return scheduleView.get().getBooked() < scheduleView.get().getSlot();
    }


    @GetMapping("/api/booking/{id}")
    public Booking getBookingById(@PathVariable(value = "id") Long id) {
        final Booking booking = bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(BOOKING_MSG, "id", id));
        return booking;
    }

    @PutMapping("/api/booking/{id}")
    public Booking updateBooking(@PathVariable(value = "id") Long id, @RequestBody Booking booking) {
        final Booking existingBooking = bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(BOOKING_MSG, "id", id));

        existingBooking.setUserAccount(booking.getUserAccount());
        existingBooking.setScheduleId(booking.getScheduleId());
        existingBooking.setBookedDate(booking.getBookedDate());
        existingBooking.setCancelDate(booking.getCancelDate());
        existingBooking.setStatus(booking.getStatus());
        existingBooking.setUpdatedDate(LocalDateTime.now());

        final Booking updatedBooking = bookingRepository.save(existingBooking);
        return updatedBooking;
    }

    @DeleteMapping(value = "/api/booking/{id}")
    public ResponseEntity<?> deleteSchedule(@PathVariable(value = "id") Long id) {
        final Booking exisitingBooking = bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(BOOKING_MSG, "id", id));
        bookingRepository.delete(exisitingBooking);
        return ResponseEntity.ok().build();
    }


}
