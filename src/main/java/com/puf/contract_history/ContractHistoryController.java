package com.puf.contract_history;

import com.puf.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
public class ContractHistoryController {

    @Autowired
    private ContractHistoryRepository contractHistoryRepository;

    @GetMapping("/api/contractHistorys")
    public List<ContractHistory> getAllContractHistories() {
        return contractHistoryRepository.findAll();
    }

    @GetMapping("/api/contractHistory")
    public Integer countAllContractHistory(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return contractHistoryRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/c")
    public ContractHistory createControntractHistoryactHistory(@Valid @RequestBody ContractHistory contractHistory) {
        return contractHistoryRepository.save(contractHistory);
    }


    @GetMapping("/api/contractHistory/{id}")
    public ContractHistory getContractHistoryById(@PathVariable(value = "id") Long id) {
        final ContractHistory contractHistory = contractHistoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("ContractHistory", "id", id));
        return contractHistory;
    }

    @PutMapping("/api/contractHistory/{id}")
    public ContractHistory updateContractHistory(@PathVariable(value = "id") Long id, @RequestBody ContractHistory contracthistory) {
        final ContractHistory existingContractHistory = contractHistoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("ContractHistory", "id", id));

        existingContractHistory.setContractId(contracthistory.getContractId());
        existingContractHistory.setUserAccountId(existingContractHistory.getUserAccountId());
        existingContractHistory.setAction(contracthistory.getAction());
        existingContractHistory.setNote(contracthistory.getNote());
        existingContractHistory.setUpdatedDate(Calendar.getInstance().getTime());

        final ContractHistory updatedContractHistory = contractHistoryRepository.save(existingContractHistory);
        return updatedContractHistory;
    }

    @DeleteMapping(value = "/api/contractHistory/{id}")
    public ResponseEntity<?> deleteContractHistory(@PathVariable(value = "id") Long id) {
        final ContractHistory exisitingContractHistory = contractHistoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("ContractHistory", "id", id));
        contractHistoryRepository.delete(exisitingContractHistory);
        return ResponseEntity.ok().build();
    }

}
