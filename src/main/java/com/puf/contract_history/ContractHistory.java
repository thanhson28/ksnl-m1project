package com.puf.contract_history;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "contract_history")
public class ContractHistory implements Serializable {

    public ContractHistory() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_account_id")
    @NotNull
    private Long userAccountId;

    @Column(name = "contract_id")
    @NotNull
    private Long contractId;

    @Column(name = "note")
    private String note;

    @Column(name = "action")
    @NotNull
    private  Integer action;

    @Column(name = "insert_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

}
