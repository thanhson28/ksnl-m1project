package com.puf.contract;


import com.puf.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
public class ContractController {

    @Autowired
    private ContractRepository contractRepository;

    @GetMapping("/api/contracts")
    public List<Contract> getAllContracts() {
        return contractRepository.findAll();
    }

    @GetMapping("/api/contract")
    public Integer countAllContract(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return contractRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/contract")
    public Contract createContract(@Valid @RequestBody Contract contract) {
        return contractRepository.save(contract);
    }


    @GetMapping("/api/contract/{id}")
    public Contract getContractById(@PathVariable(value = "id") Long contractId) {
        final Contract foundContract = contractRepository.findById(contractId).orElseThrow(() -> new ResourceNotFoundException("Contract", "id", contractId));
        return foundContract;
    }

    @PutMapping("/api/contract/{id}")
    public Contract updateContract(@PathVariable(value = "id") Long id, @RequestBody Contract contract) {
        final Contract existingContract = contractRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contract", "id", id));

        existingContract.setUserAccountId(contract.getUserAccountId());
        existingContract.setStartDate(contract.getStartDate());
        existingContract.setEndDate(contract.getEndDate());
        existingContract.setStatus(contract.getStatus());

        existingContract.setUpdatedDate(LocalDateTime.now());

        final Contract updatedContract = contractRepository.save(existingContract);
        return updatedContract;
    }

    /**
     * Delete an com.puf.actor.
     * /api/actor
     */
    @DeleteMapping(value = "/api/contract/{id}")
    public ResponseEntity<?> deleteContract(@PathVariable(value = "id") Long id) {
        final Contract exisitingContract = contractRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contract", "id", id));
        contractRepository.delete(exisitingContract);
        return ResponseEntity.ok().build();
    }

}
