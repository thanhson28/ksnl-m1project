package com.puf.contract;

/**
 * 1: Created 2: Sent 3: Approved 4: Signed 5: Rejected
 */
public enum ContractAction {

    CREATED(1), SENT(2), APPROVED(3), SIGNED(4), REJECTED(5);

    private final int number;

    private ContractAction(int numberAction) {
        this.number = numberAction;
    }

    public int getNumber() {
        return this.number;
    }
}
