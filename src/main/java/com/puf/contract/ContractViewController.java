package com.puf.contract;

import com.puf.user_account.UserAccount;
import com.puf.util.EmailService;
import com.puf.view.ContractView;
import com.puf.view.ContractViewDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ContractViewController {

    @Autowired
    private ContractViewService contractViewService;

    @Autowired
    private EmailService emailService;

    @GetMapping("/api/views/contracts")
    public List<ContractView> getAllContractViews() {
        return contractViewService.getAllContractViews();
    }

    @GetMapping("/api/views/contract/{id}")
    public ContractView getContractViewByID(@PathVariable(value = "id") Long contractViewId) {
        final ContractView foundContractView = contractViewService.getContractViewById(contractViewId);
        return foundContractView;
    }

    @PostMapping("/api/views/contract")
    public ContractViewDto createNewContractView(@Valid @RequestBody ContractViewDto contractView) {

        final UserAccount newUser = contractViewService.createNewContractView(contractView);

        //send email
        final String userName = String.format("%s %s",newUser.getFirstName(), newUser.getLastName());
        emailService.sendWelcomeEmail(userName, newUser.getEmail(), newUser.getPassword());
        //finish sending email

        return contractView;
    }


}
