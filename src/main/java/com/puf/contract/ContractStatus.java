package com.puf.contract;

/**
 * #1:Pending 2:Start 3:Finished 4:Rejected
 */
public enum ContractStatus {
    PENDING(1),
    START(2),
    FINISH(3),
    REJECTED(4);

    private int status;

    private ContractStatus(final int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
