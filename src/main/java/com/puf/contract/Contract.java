package com.puf.contract;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Slf4j
@Entity
@Table(name = "contract")
public class Contract {

    public static final int DEFAULT_MAX_LAST_ACTION = 4;

    public Contract() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_account_id")
    @NotNull
    private Long userAccountId; //This is player id

    @Column(name = "membership_id")
    @NotNull
    private Long membershipId;

    @Column(name = "contract_status")
    private Integer status;

    @Column(name = "start_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime startDate;

    @Column(name = "end_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime endDate;

    @Column(name = "last_action")
    private Integer lastAction;

    @Column(name = "note")
    private String note;

    @Column(name = "updated_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime updatedDate;

}
