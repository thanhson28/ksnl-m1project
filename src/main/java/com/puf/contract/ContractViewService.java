package com.puf.contract;

import com.puf.ResourceNotFoundException;
import com.puf.contract_history.ContractHistory;
import com.puf.contract_history.ContractHistoryRepository;
import com.puf.membership.Membership;
import com.puf.membership.MembershipRepository;
import com.puf.user_account.UserAccount;
import com.puf.user_account.UserAccountRepository;
import com.puf.view.ContractView;
import com.puf.view.ContractViewDto;
import helper.ContractViewHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class ContractViewService {

    @Autowired
    private UserAccountRepository userAccountRepo;

    @Autowired
    private MembershipRepository membershipRepo;

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractHistoryRepository contractHistoryRepo;

    @Autowired
    private ContractViewRepository contractViewRepo;

    /**
     * Fetch all contract views.
     */
    public List<ContractView> getAllContractViews() {
        return contractViewRepo.findAll();
    }

    /**
     * find contract view by contractViewId
     * @param contractViewId
     * @return
     */
    public ContractView getContractViewById(final Long contractViewId) {
        final ContractView foundContractView = contractViewRepo.findById(contractViewId).orElseThrow(() -> new ResourceNotFoundException("ContractView", "id", contractViewId));
        return foundContractView;
    }

    /**
     * create new contract view including :
     * create new user </p>
     * create new membership </p>
     * create new contract </p>
     * create new contract history </p>
     *
     * @param contractView
     */
    public UserAccount createNewContractView(final ContractViewDto contractView) {
        final UserAccount newUser = createNewUserAccount(contractView);
        final Contract newContract = createNewContract(contractView, newUser.getId(), contractView.getMembershipId());
        createNewContractHistory(newContract);

        return newUser;
    }

    private UserAccount createNewUserAccount(final ContractViewDto contractView) {
        final UserAccount userAccount = ContractViewHelper.extractUserAccount(contractView);
        final UserAccount newUser = userAccountRepo.save(userAccount);
        log.info(String.format("New user id %s has created with username %s", newUser.getUserName(), newUser.getId()));
        return newUser;
    }

    private Contract createNewContract(final ContractViewDto contractView, final Long userId, final Long membershipId) {
        final Contract contract = ContractViewHelper.extractContract(contractView, userId, membershipId);
        final Contract newContract = contractRepository.save(contract);
        log.info(String.format("New contract id %s has created", newContract.getId()));
        return newContract;
    }

    private ContractHistory createNewContractHistory(final Contract contract) {
        final ContractHistory contractHistory = new ContractHistory();
        contractHistory.setUserAccountId(contract.getUserAccountId());
        contractHistory.setContractId(contract.getId());
        contractHistory.setAction(contract.getLastAction());
        contractHistory.setNote(String.format("New contract history created with action %s", contract.getLastAction()));

        final ContractHistory newContractHistory = contractHistoryRepo.save(contractHistory);
        log.info(String.format("New contract history id %s has created", newContractHistory.getId()));
        return newContractHistory;
    }

}
