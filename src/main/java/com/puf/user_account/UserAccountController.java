package com.puf.user_account;

import com.puf.ResourceNotFoundException;
import com.puf.view.AdminInfoView;
import com.puf.view.MemberInfoView;
import com.puf.view.MemberView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class UserAccountController {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private MemberViewRepository memberViewRepo;

    @Autowired
    private MemberInfoViewRepository memberInfoViewRepo;

    @Autowired
    private AdminInfoViewRepository adminInfoViewRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Get all user accounts.
     */
    @GetMapping("/api/userAccounts")
    public List<UserAccount> getAllUserAccounts() {
        return userAccountRepository.findAll();
    }

    @GetMapping("/api/views/members")
    public List<MemberView> getAllMemberViews() {
        return memberViewRepo.findAll();
    }

    @GetMapping("/api/views/memberinfos")
    public List<MemberInfoView> getAllMemberInfoViews() {
        return memberInfoViewRepo.findAll();
    }

    @GetMapping("/api/views/admininfos")
    public List<AdminInfoView> getAllAdminInfoViews() {
        return adminInfoViewRepo.findAll();
    }

    @GetMapping("/api/userAccount")
    public Integer countAllUserAccount(@RequestParam("action") String action) {
        if ("count".equals(action)) {
            return userAccountRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such any action for this action");
    }

    /**
     * Create new {@link UserAccount}.
     */
    @PostMapping("/api/userAccount")
    public UserAccount createUserAccount(@Valid @RequestBody UserAccount userAccount) {
        final String encodedPwd = passwordEncoder.encode(userAccount.getPassword());
        userAccount.setPassword(encodedPwd);

        //save
        return userAccountRepository.save(userAccount);
    }

    @GetMapping("/api/userAccount/{id}")
    public UserAccount getUserAccountById(@PathVariable(value = "id") Integer userAccountId) {
        final UserAccount foundUserAccount = userAccountRepository.findById(Long.valueOf(userAccountId.toString())).orElseThrow(() -> new ResourceNotFoundException("UserAccount", "id", userAccountId));
        return foundUserAccount;
    }

    @PutMapping(name = "/api/userAccount/{id}")
    public UserAccount updateUserAccount(final @PathVariable(value = "id") Integer id, @RequestBody UserAccount userAccount) {
        final UserAccount existingUserAccount = userAccountRepository.findById(Long.valueOf(id.toString())).orElseThrow(() -> new ResourceNotFoundException("UserAccount", "id", id));

        updateUserAccount(userAccount, existingUserAccount);

        final UserAccount updatedUserAccount = userAccountRepository.save(existingUserAccount);
        return updatedUserAccount;
    }

    /**
     * Update existing user with new information.
     *
     * @param userAccount
     * @param existingUserAccount
     */
    private void updateUserAccount(final @RequestBody UserAccount userAccount, final UserAccount existingUserAccount) {
        existingUserAccount.setAddress(userAccount.getAddress());
        existingUserAccount.setCity(userAccount.getCity());
        existingUserAccount.setCountry(userAccount.getCountry());
        existingUserAccount.setDateOfBirth(userAccount.getDateOfBirth());
        existingUserAccount.setEmail(userAccount.getEmail());
        existingUserAccount.setFirstName(userAccount.getFirstName());
        existingUserAccount.setLastName(userAccount.getLastName());

        existingUserAccount.setPassword(userAccount.getPassword());
        existingUserAccount.setCategory(userAccount.getCategory());

        existingUserAccount.setUpdatedDate(LocalDateTime.now());
    }


    @DeleteMapping(value = "/api/userAccount/{id}")
    public ResponseEntity<?> deleteUserAccount(@PathVariable(value = "id") Integer id) {
        final UserAccount existingUserAccount = userAccountRepository.findById(Long.valueOf(id.toString())).orElseThrow(() -> new ResourceNotFoundException("UserAccount", "id", id));
        userAccountRepository.delete(existingUserAccount);
        return ResponseEntity.ok().build();
    }

}
