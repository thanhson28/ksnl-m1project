package com.puf.user_account;

import com.puf.view.AdminInfoView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminInfoViewRepository extends JpaRepository<AdminInfoView, Long> {
}
