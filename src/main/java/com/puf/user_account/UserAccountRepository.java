package com.puf.user_account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    Optional<UserAccount> findByUserName(String userName);

    @Query(value = "SELECT u FROM UserAccount u where u.email = ?1 and u.password = ?2 ")
    Optional<UserAccount> login(String email,String password);

    Optional<UserAccount> findByToken(String token);
}
