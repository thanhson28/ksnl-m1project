package com.puf.user_account;

import com.puf.view.MemberInfoView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberInfoViewRepository extends JpaRepository<MemberInfoView, Long> {
}
