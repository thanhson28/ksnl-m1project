package com.puf.user_account;

import com.puf.converter.DateConverter;
import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Slf4j
@Entity
@Table(name="user_account")
public class UserAccount implements Serializable {

    public static final int VALID_STATUS = 1;
    public static final int INVALID_STATUS = 2;

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    @NotBlank
    private String userName;

    @Column(name = "email")
    @NotBlank
    private String email;

    @Column(name = "password")
    @NotBlank
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_birth")
    @Convert(converter = DateConverter.class)
    private LocalDate dateOfBirth;

    @Column(name = "phone")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "valid")
    private Integer valid = 1;

    @Column(name = "token")
    private String token;

    @Column(name = "category")
    private Integer category = 1;

    @Column(name = "insert_timestamp")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime updatedDate;

}
