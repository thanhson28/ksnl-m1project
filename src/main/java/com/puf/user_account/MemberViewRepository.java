package com.puf.user_account;

import com.puf.view.MemberView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberViewRepository extends JpaRepository<MemberView, Long> {
}
