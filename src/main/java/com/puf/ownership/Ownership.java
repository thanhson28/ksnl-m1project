package com.puf.ownership;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "ownership")
public class Ownership implements Serializable {

    public Ownership() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    @NotBlank
    private String type;

    @Column(name = "price")
    @NotBlank
    private BigDecimal price;

    @Column(name = "note")
    @NotBlank
    private String note;

    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

}