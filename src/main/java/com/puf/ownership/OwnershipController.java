package com.puf.ownership;

import com.puf.ResourceNotFoundException;
import com.puf.membership.Membership;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
public class OwnershipController {
    @Autowired
    private OwnershipRepository ownershipRepository;

    @GetMapping("/api/ownerships")
    public List<Ownership> getAllMemberships() {
        return ownershipRepository.findAll();
    }

    @GetMapping("/api/ownership")
    public Integer countAllMembership(@RequestParam("action") String action) {
        if("count".equals(action)) {
            return ownershipRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/ownership")
    public Ownership createOwnership(@Valid @RequestBody Ownership ownership) {
        return ownershipRepository.save(ownership);
    }


    @GetMapping("/api/ownership/{id}")
    public Ownership getOwnershipById(@PathVariable(value =  "id") Long membershipId) {
        final Ownership foundOwnership = ownershipRepository.findById(membershipId).orElseThrow(() -> new ResourceNotFoundException("Ownership", "id", membershipId));
        return foundOwnership;
    }

    @PutMapping("/api/ownership/{id}")
    public Ownership updateOwnership(@PathVariable(value = "id") Long id, @RequestBody Ownership ownership) {
        final Ownership existingOwnership = ownershipRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ownership", "id", id));
        existingOwnership.setType(ownership.getType());
        existingOwnership.setPrice(ownership.getPrice());
        existingOwnership.setNote(ownership.getNote());
        existingOwnership.setUpdatedDate(Calendar.getInstance().getTime());

        final Ownership updatedOwnership = ownershipRepository.save(existingOwnership);
        return updatedOwnership;
    }

    /**
     * Delete an com.puf.ownership.
     * /api/ownership
     */
    @DeleteMapping(value = "/api/ownership/{id}")
    public ResponseEntity<?> deleteOwnership(@PathVariable(value = "id") Long id) {
        final Ownership exisitingOwnership = ownershipRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ownership", "id", id));
        ownershipRepository.delete(exisitingOwnership);
        return ResponseEntity.ok().build();
    }
}
