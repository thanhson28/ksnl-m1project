package com.puf.converter;

import javax.persistence.AttributeConverter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate attribute) {
        return attribute != null ? Date.from(attribute.atStartOfDay(ZoneId.systemDefault()).toInstant()) : Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public LocalDate convertToEntityAttribute(Date dbData) {
        return dbData != null ? dbData.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() : null;
    }
}
