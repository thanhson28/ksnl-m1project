package com.puf.center;

import java.io.Serializable;
import java.util.stream.Stream;

public enum CenterStatus implements Serializable {

    AVAILABLE(1),
    MAINTENANCE(2),
    CLOSED(3),
    COMMING_SOON(4);

    private int number;

    CenterStatus(final int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public static CenterStatus of(final int number) {
        return Stream.of(CenterStatus.values())
                .filter(c -> c.getNumber() == number)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
