package com.puf.center;

import com.puf.view.CenterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CenterViewRepository extends JpaRepository<CenterView, Long> {
}
