package com.puf.center;

import com.puf.converter.DateTimeConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Slf4j
@Entity
@Table(name = "center")
public class Center implements Serializable {

    public Center() {

    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    @NotBlank
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "time_open")
    private String openTime;

    @Column(name = "time_close")
    private String closeTime;

    @Column(name = "phone")
    private String phone;

    @Column(name = "website")
    private String website;

    @Column(name = "user_account_id")
    private Long ownerId;

    @Column(name = "create_date")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime createDate;

    @Column(name = "category")
    private Integer category;

    @Column(name = "status_id")
    private Integer status;

    @Column(name = "valid")
    private Integer validType = 1;

    @Column(name = "insert_timestamp")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime updatedDate = LocalDateTime.now();

}
