package com.puf.center;

import com.puf.ResourceNotFoundException;
import com.puf.exception.SportClubError;
import com.puf.login.UserAccountService;
import com.puf.user_account.UserAccount;
import com.puf.view.CenterView;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class CenterController {

    @Autowired
    private CenterRepository centerRepository;

    @Autowired
    private CenterViewRepository centerViewRepository;

    @Autowired
    private UserAccountService userAccountService;

    @GetMapping("/api/centers")
    public List<Center> getAllCenters() {
        return centerRepository.findAll();
    }

    @GetMapping("/api/center")
    public List<Center> getCentersByOwner(@RequestParam("action") String action, @RequestHeader HttpHeaders headers) {
        final Optional<UserAccount> userAccount = userAccountService.getUserAccountFromTokenHeader(headers);
        if(!userAccount.isPresent()) {
            log.debug("Not found any user account");
            return Collections.emptyList();
        }

        final List<Center> existingCenters = centerRepository.findByOwnerId(userAccount.get().getId());
        if(CollectionUtils.isEmpty(existingCenters)) {
            log.debug(String.format("Not found any center by user %s", userAccount.get().getUserName()));
            return Collections.emptyList();
        }
        return existingCenters;
    }

    @GetMapping("/api/views/centers")
    public List<CenterView> getAllBookingViews() {
        return centerViewRepository.findAll();
    }

//    @GetMapping("/api/center")
//    public Integer countAllCenter(@RequestParam("action") String action) {
//        if ("count".equals(action)) {
//            return centerRepository.findAll().size();
//        }
//        throw new IllegalArgumentException("Not such an any action for this action");
//    }

    @PostMapping("/api/center")
    public Center createCenter(@Valid @RequestBody Center center) {
        return centerRepository.save(center);
    }


    @GetMapping("/api/center/{id}")
    public Center getCenterById(@PathVariable(value = "id") Integer centerId) {
        final Center foundCenter = centerRepository.findById(centerId).orElseThrow(() -> new ResourceNotFoundException("Center", "id", centerId));
        return foundCenter;
    }

    @PutMapping("/api/center/{id}")
    public Center updateCenter(@PathVariable(value = "id") Integer id, @RequestBody Center center) {
        final Center existingCenter = centerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Center", "id", id));
        existingCenter.setValidType(center.getValidType());
        existingCenter.setName(center.getName());
        existingCenter.setAddress(center.getAddress());
        existingCenter.setCity(center.getCity());
        existingCenter.setCountry(center.getCountry());
        existingCenter.setOpenTime(center.getOpenTime());
        existingCenter.setCloseTime(center.getCloseTime());
        existingCenter.setPhone(center.getPhone());
        existingCenter.setOwnerId(center.getOwnerId());
        existingCenter.setCreateDate(center.getCreateDate());
        existingCenter.setStatus(center.getStatus());
        existingCenter.setUpdatedDate(LocalDateTime.now());

        //store
        final Center updatedCenter = centerRepository.save(existingCenter);
        return updatedCenter;
    }

    /**
     * Delete an com.puf.actor.
     * /api/actor
     */
    @DeleteMapping(value = "/api/center/{id}")
    public ResponseEntity<?> deleteCenter(@PathVariable(value = "id") Integer id) {
        final Center exisitingCenter = centerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Center", "id", id));
        centerRepository.delete(exisitingCenter);
        return ResponseEntity.ok().build();
    }
}
