package com.puf.sport;

import com.puf.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Slf4j
@RestController
public class SportController {

    @Autowired
    private SportRepository sportRepository;

    @GetMapping("/sports")
    public List<Sport> getAllSports() {
        return sportRepository.findAll();
    }

    @GetMapping("/api/sport")
    public Integer countAllSport(@RequestParam("action") String action) {
        if("count".equals(action)) {
            return sportRepository.findAll().size();
        }
        throw new IllegalArgumentException("Not such an any action for this action");
    }

    @PostMapping("/api/sport")
    public Sport createSport(@Valid @RequestBody Sport sport) {
        return sportRepository.save(sport);
    }


    @GetMapping("/api/sport/{id}")
    public Sport getSportById(@PathVariable(value =  "id") Long sportId) {
        final Sport foundSport = sportRepository.findById(sportId).orElseThrow(() -> new ResourceNotFoundException("Sport", "id", sportId));
        return foundSport;
    }

    @PutMapping("/api/sport/{id}")
    public Sport updateSport(@PathVariable(value = "id") Long id, @RequestBody Sport sport) {
        final Sport existingSport = sportRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Sport", "id", id));
        existingSport.setName(sport.getName());
        existingSport.setSlot(sport.getSlot());
        existingSport.setUpdatedDate(Calendar.getInstance().getTime());

        final Sport updatedPlayer = sportRepository.save(existingSport);
        return updatedPlayer;
    }

    /**
     * Delete an com.puf.sport.
     * /api/player
     */
    @DeleteMapping(value = "/api/sport/{id}")
    public ResponseEntity<?> deleteSport(@PathVariable(value = "id") Long id) {
        final Sport exisitingSport = sportRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Sport", "id", id));
        sportRepository.delete(exisitingSport);
        return ResponseEntity.ok().build();
    }

}
