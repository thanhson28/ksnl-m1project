<html>
    <body>
        <h3>Welcome to KSNL Sport Club</h3>
        <p>
            Hello <b> ${user_name} </b> , You've joined KSNL Sport Club. <br/>
            Thank you for choosing us. We wish you have a pleasant experience with our services. <br/>
            Please use your personal information as below to login to start using our services on the KSNL Sport Club application <br/>
            User name: ${user_email} <br/>
            Password:  ${password}   <br/>

            Please click on the below link or copy paste it on to your browser: <a href="${link}" > Link to </a> <br/>
        </p>

        <p>
            This email has been generated automatically, please do not reply.
Please beware of any other party that uses KSNL Sport Club's name to distribute vouchers or asks for your personal data. KSNL Sport Club never requests your password or personal data through email, personal message or any other media.
Have any questions ? Contact us at: support_ksnl_sport@gmail.com

        </p>

        <p>
            Best regards, <br/>
            Support Team - KSNL Sport Club

        </p>

    </body>
</html>