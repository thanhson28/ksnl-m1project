#View of Admin Info
CREATE OR REPLACE VIEW sport_club.admin_info_view AS 
SELECT ua.first_name
, ua.last_name
, ua.date_of_birth
, ua.phone
, ua.address
, ua.city
, ua.country
, role.role_name
, tg.team_group_name
FROM user_account ua
LEFT JOIN user_account_group uag ON uag.user_account_id = ua.id
LEFT JOIN role ON role.id = uag.role_id
LEFT JOIN team_group tg ON tg.id = uag.team_group_id
WHERE ua.id = 1;


#View of Member
CREATE OR REPLACE VIEW sport_club.member_view AS 
SELECT ua.id
, ua.first_name
, ua.last_name
, ua.date_of_birth
, ua.phone
, ua.address
, ua.city
, ua.country
, CASE  WHEN ua.valid = 1 THEN "Active"
		WHEN ua.valid = 2 THEN "Inactive"
  END AS valid
, ua.insert_timestamp as created_date
FROM user_account ua;


#View of Contract

CREATE OR REPLACE VIEW sport_club.contract_view AS 
SELECT con.id as contract_id
, con.user_account_id
, ua.first_name
, ua.last_name
, ua.email
, ua.date_of_birth
, ua.phone
, ua.address
, ua.city
, ua.country
, mem.id AS membership_id
, mem.type AS membership_type
, mem.card_level
, mem.session
, mem.price
, mem.currency
, con.start_date
, con.end_date
, chc.insert_timestamp as created_date
, CONCAT(uac.first_name," ",uac.last_name) as created_by
, chs.insert_timestamp as sent_date
, CONCAT(uas.first_name," ",uas.last_name) as sent_by
, cha.insert_timestamp as approved_date
, CONCAT(uaa.first_name," ",uaa.last_name) as approved_by
, chsign.insert_timestamp as signed_date
, CONCAT(uasign.first_name," ",uasign.last_name) as signed_by
, chr.insert_timestamp as rejected_date
, CONCAT(uar.first_name," ",uar.last_name) as rejected_by
FROM sport_club.contract con
INNER JOIN user_account ua on con.user_account_id = ua.id
LEFT JOIN sport_club.contract_history chc ON chc.contract_id = con.id AND chc.action = 1
LEFT JOIN user_account uac on chc.user_account_id = uac.id #Created
LEFT JOIN sport_club.contract_history chs ON chs.contract_id = con.id AND chs.action = 2
LEFT JOIN user_account uas on chs.user_account_id = uas.id #Sent
LEFT JOIN sport_club.contract_history cha ON cha.contract_id = con.id AND cha.action = 3
LEFT JOIN user_account uaa on cha.user_account_id = uaa.id #Approved
LEFT JOIN sport_club.contract_history chsign ON chsign.contract_id = con.id AND chsign.action = 4
LEFT JOIN user_account uasign on chsign.user_account_id = uasign.id #Signed
LEFT JOIN sport_club.contract_history chr ON chr.contract_id = con.id AND chr.action = 5
LEFT JOIN user_account uar on chr.user_account_id = uar.id #Rejected
INNER JOIN membership mem on con.membership_id = mem.id;

#View of membership

CREATE OR REPLACE VIEW sport_club.membership_view AS 
SELECT mem.id as membership_id
, mem.type
, mem.card_level
, mem.session
, mem.price
, mem.currency
, mem.note
FROM membership mem;

#View of Member Info --User login only

CREATE OR REPLACE VIEW sport_club.member_info_view AS 
SELECT ua.first_name
, ua.last_name
, ua.date_of_birth
, ua.phone
, ua.address
, ua.city
, ua.country
, mem.type AS membership_type
, mem.card_level
, mem.session
, mem.price
, mem.currency
, con.start_date
, con.end_date
, total_contract
FROM user_account ua
LEFT JOIN contract con on con.user_account_id = ua.id
LEFT JOIN membership mem on con.membership_id = mem.id
LEFT JOIN (SELECT user_account_id,COUNT(*) AS total_contract FROM contract 
GROUP BY contract.user_account_id) as tc ON tc.user_account_id = ua.id;

#View of List of center--Owner login only

CREATE OR REPLACE VIEW sport_club.center_view AS 
SELECT cen.id
, cen.name
, cen.time_open
, cen.time_close
, cen.phone
, cen.website
, cen.address
, cen.city
, cen.country
, CASE  WHEN cen.valid = 1 THEN "Active"
		WHEN cen.valid = 2 THEN "Inactive"
  END AS valid
, cys.status_name
FROM center cen
INNER JOIN user_account ua on cen.user_account_id = ua.id
INNER JOIN center_yard_status cys on cys.id = cen.status_id;

#View of List of yard --Owner login only

CREATE OR REPLACE VIEW sport_club.yard_view AS 
SELECT yard.id as yard_id
, yard.name as yard_name
, yard.sport_id as sport_id
, yard.center_id as center_id
, sport.name as sport_name
, sport.slot
, cen.user_account_id AS user_account_id
, cen.name as center_name
, cen.time_open
, cen.time_close
, cen.phone
, cen.website
, cen.address
, cen.city
, cen.country
, CASE  WHEN yard.valid = 1 THEN "Active"
		WHEN yard.valid = 2 THEN "Inactive"
  END AS valid
, cys.status_name
FROM yard
INNER JOIN center cen on cen.id = yard.center_id 
INNER JOIN user_account ua on cen.user_account_id = ua.id
INNER JOIN center_yard_status cys on cys.id = yard.status_id
INNER JOIN sport on sport.id = yard.sport_id;

#View of Schedule

CREATE OR REPLACE VIEW sport_club.schedule_view AS 
SELECT sch.id AS schedule_id
, yard.id as yard_id
, yard.name as yard_name
, sport.name as sport_name
, sport.slot
, slot_booked.booked
, sport.image as sport_image
, sch.schedule_date
, sch.start_time
, sch.end_time
, CASE  WHEN sch.schedule_status = 1 THEN "Scheduled"
		WHEN sch.schedule_status = 2 THEN "Cancelled"
        WHEN sch.schedule_status = 3 THEN "Fulled"
        WHEN sch.schedule_status = 4 THEN "Finished"
  END AS schedule_status
, cen.name as center_name
, cen.phone
, cen.website
, cen.address
, cen.city
, cen.country
FROM schedule sch
INNER JOIN yard on yard.id = sch.yard_id
INNER JOIN center cen on cen.id = yard.center_id 
INNER JOIN sport on sport.id = yard.sport_id
LEFT JOIN (SELECT schedule_id, 
COUNT(*) as booked 
FROM booking 
WHERE booking_status = 1 GROUP BY schedule_id) 
as slot_booked on slot_booked.schedule_id = sch.id;

#View of Booking

CREATE OR REPLACE VIEW sport_club.booking_view AS 
SELECT book.id as booking_id
, ua.id as player_id
, cen.user_account_id as owner_id
, CONCAT(ua.first_name," ",ua.last_name) as user_name
, yard.id as yard_id
, yard.name as yard_name
, sport.name as sport_name
, sport.slot
, slot_booked.booked
, sport.image as sport_image
, sch.schedule_date
, sch.start_time
, sch.end_time
, cen.name as center_name
, cen.phone
, cen.website
, cen.address
, cen.city
, cen.country
, book.booking_status
FROM booking book
INNER JOIN user_account ua on book.user_account_id=ua.id
INNER JOIN schedule sch on sch.id = book.schedule_id
INNER JOIN yard on yard.id = sch.yard_id
INNER JOIN center cen on cen.id = yard.center_id
INNER JOIN sport on sport.id = yard.sport_id
LEFT JOIN (SELECT schedule_id,
COUNT(*) as booked FROM booking
WHERE booking_status = 1 GROUP BY schedule_id)
as slot_booked on slot_booked.schedule_id = book.schedule_id;

#select * from contract_view


