CREATE OR REPLACE VIEW sport_club.admin_report_view AS 
SELECT contract_player_1.month_key
, contract_player_1.no_player
, contract_player_1.fee_player
, contract_player_1.avg_fee_player
, contract_owner_1.no_owner
, contract_owner_1.fee_owner
, contract_owner_1.avg_fee_owner
FROM (
SELECT first_day as month_key
, COUNT(*) as no_player 
, SUM(price) as fee_player
, ROUND(AVG(price),2) as avg_fee_player
, 1 as join_key
FROM (
SELECT Date_Format(date_add(con.insert_timestamp,interval -DAY(con.insert_timestamp)+1 DAY),'%m/%d/%Y') AS first_day
, con.id as contract_id
, con.user_account_id
, ua.category
, mem.type AS membership_type
, mem.card_level
, mem.session
, mem.price
, mem.currency

FROM sport_club.contract con
INNER JOIN user_account ua on con.user_account_id = ua.id
INNER JOIN membership mem on con.membership_id = mem.id
WHERE ua.category = 2) as contract_player
GROUP BY first_day) as contract_player_1
LEFT JOIN
(
SELECT first_day as month_key
, COUNT(*) as no_owner
, SUM(price) as fee_owner
, ROUND(AVG(price),2) as avg_fee_owner
, 1 as join_key
FROM (
SELECT Date_Format(date_add(con.insert_timestamp,interval -DAY(con.insert_timestamp)+1 DAY),'%m/%d/%Y') AS first_day
, con.id as contract_id
, con.user_account_id
, ua.category
, mem.type AS membership_type
, mem.card_level
, mem.session
, mem.price
, mem.currency

FROM sport_club.contract con
INNER JOIN user_account ua on con.user_account_id = ua.id
INNER JOIN membership mem on con.membership_id = mem.id
WHERE ua.category = 3) as contract_owner
GROUP BY first_day) as contract_owner_1 ON contract_player_1.join_key = contract_owner_1.join_key;

CREATE OR REPLACE VIEW sport_club.sum_admin_report_view AS
    SELECT 
        month_key,
        no_player AS sum_no_player,
        no_owner AS sum_no_owner,
        fee_player AS sum_fee_player,
        fee_owner as sum_fee_owner,
        avg_fee_player as sum_avg_fee_player,
        avg_fee_owner as sum_avg_fee_owner
        
    FROM
        admin_report_view
    WHERE
        month_key = DATE_FORMAT(DATE_ADD(NOW(),
                    INTERVAL - DAY(NOW()) + 1 DAY),
                '%m/%d/%Y');
                
CREATE OR REPLACE VIEW sport_club.admin_country AS
SELECT all_country.country
,country_player.no_player
,country_owner.no_owner
FROM
(SELECT DISTINCT ua.country
FROM user_account ua) AS all_country
LEFT JOIN
(
SELECT  ua.country
, COUNT(*) as no_player
FROM user_account ua
WHERE ua.category = 2
GROUP BY ua.country) AS country_player ON all_country.country = country_player.country
LEFT JOIN
(
SELECT  ua.country
, COUNT(*) as no_owner
FROM user_account ua
WHERE ua.category = 3
GROUP BY ua.country) AS country_owner ON all_country.country = country_owner.country
;

CREATE OR REPLACE VIEW sport_club.yard_owner_report_view AS 
SELECT first_day, yard_id, yard_name,owner_id, no_booking
FROM (
SELECT first_day, yard_id, yard_name,owner_id, COUNT(*) AS no_booking
FROM
( 
SELECT book.id as booking_id
, ua.id as player_id
, cen.user_account_id as owner_id
, yard.id as yard_id
, yard.name as yard_name
, date_Format(date_add(sch.schedule_date,interval -DAY(sch.schedule_date)+1 DAY),'%m/%d/%Y') AS first_day
, book.booking_status
, 1 as join_key
FROM booking book
INNER JOIN user_account ua on book.user_account_id=ua.id
INNER JOIN schedule sch on sch.id = book.schedule_id
INNER JOIN yard on yard.id = sch.yard_id
INNER JOIN center cen on cen.id = yard.center_id
INNER JOIN sport on sport.id = yard.sport_id
WHERE book.booking_status = 1
AND cen.user_account_id  = 6) as top_yard
GROUP BY first_day, yard_id, yard_name,owner_id ) top_yard
ORDER BY  no_booking;

CREATE OR REPLACE VIEW sport_club.owner_report_view AS 
SELECT first_day, owner_id, no_player_owner
FROM (
SELECT first_day,owner_id, COUNT(*) AS no_player_owner
FROM
( 
SELECT book.id as booking_id
, ua.id as player_id
, cen.user_account_id as owner_id
, yard.id as yard_id
, yard.name as yard_name
, date_Format(date_add(sch.schedule_date,interval -DAY(sch.schedule_date)+1 DAY),'%m/%d/%Y') AS first_day
, book.booking_status
, 1 as join_key
FROM booking book
INNER JOIN user_account ua on book.user_account_id=ua.id
INNER JOIN schedule sch on sch.id = book.schedule_id
INNER JOIN yard on yard.id = sch.yard_id
INNER JOIN center cen on cen.id = yard.center_id
INNER JOIN sport on sport.id = yard.sport_id
WHERE book.booking_status = 1
AND cen.user_account_id  = 6) as top_yard
GROUP BY first_day,owner_id ) top_yard
ORDER BY  first_day;
