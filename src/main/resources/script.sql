CREATE TABLE user_account (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username)
);

CREATE TABLE user_roles (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES user_account (username)
);

--Insert data into user_account
INSERT INTO user_account(username,password,enabled)
    VALUES ('thanhson','123456', true),
            ('alex','123456', true);

--Insert data into user_roles
INSERT INTO user_roles (username, role)
        VALUES ('thanhson', 'ROLE_USER'),
             ('thanhson', 'ROLE_ADMIN'),
             ('alex', 'ROLE_USER');