<html>
    <body>
        <h3><Booking in KSNL Sport Club </h3>
            <p>
                    Hello, ${username} You're successful to create booking in KSNL Sport Club. Here is your information: <br/>
                    Center: ${center_name} <br/>
                    Yard: ${yard_name} <br/>
                    Sport: ${sport_name} <br/>
                    Date: ${schedule_date} <br/>
                    Start time: ${start_time} <br/>
                    End time: ${end_time} <br/>
                   <a href=${link}> Go to KSNL Sport Club </a> <br/>
            </p>

            <p>
                    This email has been generated automatically, please do not reply. <br/>
                    Please beware of any other party that uses KSNL Sport Club's name to distribute vouchers or asks for your personal data. KSNL Sport Club never requests your password or personal data through email, personal message or any other media.
                    Have any questions ?  <br/>
                    Contact us at: support_ksnl_sport@gmail.com <br/>
                    Best regards, <br/>
                    Support Team - KSNL Sport Club
            </p>

    </body>
</html>